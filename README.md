# Data Listener Server for IoT devices

This is a simple Flask web server that listens for POST requests containing JSON data and writes the data to a CSV file with a timestamp. The program uses the Gunicorn WSGI server for deployment.

## Usage

1. **Clone the Repository:**

   ```bash
   git clone https://gitlab.com/faulTTY/data-listener.git
   cd data-listener

2. **Build and Run the Docker Container:**

```bash
docker-compose up --build -d
```

3. **Send POST Requests:**

```bash
curl -X POST -H "Content-Type: application/json" -d '{"loudness": 42, "humidity": 51, "temp": 60}' http://127.0.0.1:5000/receive_data
```
The received data will be appended to the data/data.csv file in the same directory as the script.

## Project Structure

- web_server.py: Flask application script.
- gunicorn_config.py: Gunicorn configuration file.
- requirements.txt: List of Python dependencies.
- data/data.csv: CSV file where the received data is stored.
- Dockerfile: Docker configuration file for building the container.
- docker-compose.yaml: Docker Compose configuration for running the container.

## Configuration

Adjust the number of Gunicorn workers and other settings based on your application's needs.


## Issues and Contributions

If you encounter any issues or have suggestions for improvements, feel free to open an issue or submit a pull request.

Happy coding! 🚀