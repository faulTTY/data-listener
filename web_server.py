from flask import Flask, request
import json
import csv
from datetime import datetime

app = Flask(__name__)

@app.route('/receive_data', methods=['POST'])
def receive_data():
    try:
        data = request.json
        if 'loudness' in data and 'humidity' in data and 'temp' in data:
            loudness = data['loudness']
            humidity = data['humidity']
            temp = data['temp']
            timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

            # Write data to CSV file
            with open('data/data.csv', 'a', newline='') as csvfile:
                fieldnames = ['Timestamp', 'Loudness', 'Humidity [%]', 'Temperature [C]']
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

                # Check if the file is empty, then write header
                if csvfile.tell() == 0:
                    writer.writeheader()

                writer.writerow({'Timestamp': timestamp, 'Loudness': loudness, 'Humidity [%]': humidity, 'Temperature [C]': temp})

            return 'Data received and written to CSV file.'
        else:
            return 'Invalid data format. Expected JSON with keys "loudness", "humidity" and "temp".', 400
    except Exception as e:
        return f'Error: {str(e)}', 500

if __name__ == '__main__':
    app.run(debug=True)
